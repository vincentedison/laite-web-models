<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\General;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];
  
  const TYPE = [
    1 => "Term & Condition",
    2 => "Privacy",
    3 => "About",
  ];

  const TERM = 1;
  const PRIVACY = 2;
  const ABOUT = 3;
  
  public function getTypeLabelAttribute()
  {
    return self::TYPE[$this->type];
  }
}

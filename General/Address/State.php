<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\General\Address;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function cities()
  {
      return $this->hasMany('App\Models\General\Address\City', 'state_id');
  }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\General\Address;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];  
  
  public function subdistricts()
  {
      return $this->hasMany('App\Models\General\Address\Subdistrict', 'city_id');
  }  

  public function state()
  {
      return $this->belongsTo('App\Models\General\Address\State', 'state_id');
  }
}

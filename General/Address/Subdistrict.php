<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\General\Address;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function city()
  {
      return $this->belongsTo('App\Models\General\Address\City', 'city_id');
  }
  
  public function getFullNameAttribute()
  {
    return $this->name.', ' .$this->city->name. ', '.$this->city->state->name;
  }
}

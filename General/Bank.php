<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\General;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];  
  
  public function getActiveLabelAttribute()
  {
    return $this->active  == 1 ? "active" : "inactive";
  }

  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }
}

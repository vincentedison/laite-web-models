<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Setting extends Model
{
    protected $fillable = [
        's_value',
    ];

    public function scopeVisible($query) {
        return $query->where('visible', 1);
    }

    public function scopeKey($query, $key, $default = null, $label = '', $type = 'string', $visible = true)
    {
        if(Cache::has('global_setting'. $key)) {
            return Cache::get('global_setting'. $key);
        }

        $setting = $query->where('s_key', $key)->first();
        if($setting) {
            Cache::put('global_setting'. $key, $setting->s_value, 1);
            return $setting->s_value;
        }
        if($default !== null) {
            $setting = new Setting;
            $setting->s_key = $key;
            $setting->s_value = $default;
            $setting->visible = $visible;
            $setting->s_type = $type;
            $setting->s_label = $label;
            $setting->save();
            Cache::put('global_setting'. $key, $setting->s_value, 1);
            return $setting->s_value;
        }
        return '';
    }

    public function scopeSetValue($query, $key, $value) {
        $setting = $query->where('s_key', $key)->first();
        if(!$setting) {
            $setting = new Setting;
            $setting->s_key = $key;
        }
        $setting->s_value = $value;
        $setting->save();
        Cache::put('global_setting'. $setting->s_key, $setting->s_value, 2);
    }
}

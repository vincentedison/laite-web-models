<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Customer;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Order\Order;
use H;
use App\Models\Commission\CommissionHistory;
use App\Models\Point\PointHistory;
use App\Models\Bonus\BonusHistory;

class Customer extends Authenticatable
{
  use HasApiTokens, LoggableModel, Notifiable;
  
  protected $guarded = [];
  protected $visible = ['id'];

  const CUSTOMER_TYPE = [
    1 => "End User",
    2 => "Reseller",
  ];

  const CUSTOMER_TYPE_END_USER = 1;
  const CUSTOMER_TYPE_RESELLER = 2;

  protected static function boot()
  {
      parent::boot();

      static::creating(function($object) {
        if($object->referral_code == null) {        
            while(true) {
                $number_candidate = H::generateNumber(8);
                if(self::where('referral_code', $number_candidate)->count() == 0) {                        
                    $object->referral_code = $number_candidate;
                    break;
                }
            }

            while(true) {
                $number_candidate = H::generateNumber(8);
                if(self::where('customer_number', $number_candidate)->count() == 0) {                        
                    $object->customer_number = $number_candidate;
                    break;
                }
            }
        }
        if($object->mobile_token == null) {        
            while(true) {
                $number_candidate = H::generateNumber(6);
                if(self::where('mobile_token', $number_candidate)->count() == 0) {                        
                    $object->mobile_token = $number_candidate;
                    break;
                }
            }
        }
      });

      static::saving(function($object) {
          if($object->isDirty('reference_code') && !empty($object->reference_code)) {
              $refery_customer = self::where('referral_code', $object->reference_code)->first();
              if($refery_customer) {
                  $object->reference_id = $refery_customer->id;
              }
          }
      });
  }

  public function findForPassport($username) {  
    return $this->whereEmail($username)->first();
  }
  
  public function bank_accounts()
  {
      return $this->hasMany('App\Models\Customer\BankAccount', 'customer_id');
  }

  public function addresses()
  {
      return $this->hasMany('App\Models\Customer\CustomerAddress', 'customer_id');
  }

  public function discussions()
  {
      return $this->hasMany('App\Models\Forum\Discussion', 'customer_id');
  }

  public function getCustomerTypeLabelAttribute()
  {
      return self::CUSTOMER_TYPE[$this->customer_type];
  }

  public function getGenderLabelAttribute()
  {
      if($this->gender == null){
          return '-';
      }
      return $this->gender == 1 ? 'Male' : 'Female';
  }

  public function getResellerAttribute()
  {
        if($this->parent == null)
            return null;

        $customer = clone $this;
        while(true) {
            $customer_parent = $customer->parent;

            if($customer_parent == null)
                return null;

            if($customer_parent->customer_type == self::CUSTOMER_TYPE_RESELLER)
                return $customer_parent;

            $customer = clone $customer_parent;
        }
  }

  public function carts()
  {
      return $this->hasMany('App\Models\Order\Cart', 'customer_id');
  }

  public function current_order()
  {
      return $this->hasOne('App\Models\Order\Order', 'customer_id')->where('order_state', Order::STATE_NEW);
  }

  public function orders()
  {
      return $this->hasMany('App\Models\Order\Order', 'customer_id');
  }

  public function parent()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'reference_id');
  }

  public function childs()
  {
    return $this->hasMany('App\Models\Customer\Customer', 'reference_id');
  }

  public function getChildrensAttribute()
  {
    if ($this->customer_type == self::CUSTOMER_TYPE_RESELLER) {
        return $this->childs;
    }
    return null;
  }

  public function commission_histories()
  {
      return $this->hasMany('App\Models\Commission\CommissionHistory', 'customer_id');
  }

  public function commission_withdrawals()
  {
      return $this->hasMany('App\Models\Commission\CommissionWithdrawal', 'customer_id');
  }
  
  public function bonus_histories()
  {
      return $this->hasMany('App\Models\Bonus\BonusHistory', 'customer_id');
  }

  public function bonus_exchanges()
  {
      return $this->hasMany('App\Models\Bonus\BonusExchange', 'customer_id');
  }

  public function point_histories()
  {
      return $this->hasMany('App\Models\Point\PointHistory', 'customer_id');
  }

  public function point_exchanges()
  {
      return $this->hasMany('App\Models\Point\PointExchange', 'customer_id');
  }

  public function product_wishlists()
  {
      return $this->belongsToMany('App\Models\Product\Product', 'wishlists', 'customer_id', 'product_id');
  }

  public function product_reviews()
  {
      return $this->hasMany('App\Models\Product\ProductReview', 'customer_id')->whereNotNull('product_id');
  }

  public function setCommissionHistory($amount, $description, $originator)
  {
    $history = new CommissionHistory;
    $history->customer_id = $this->id;
    $history->description = $description;
    $history->amount = $amount;
    $originator->commission_histories()->save($history);
  }

  public function setPointHistory($amount, $description, $originator)
  {
    $history = new PointHistory;
    $history->customer_id = $this->id;
    $history->description = $description;
    $history->amount = $amount;
    $originator->point_histories()->save($history);
  }

  public function setBonusHistory($amount, $description, $originator)
  {
    $history = new BonusHistory;
    $history->customer_id = $this->id;
    $history->description = $description;
    $history->amount = $amount;
    $originator->bonus_histories()->save($history);
  }

  public function sessions()
  {
      return $this->hasMany('App\Models\Ticket\Session', 'customer_id');
  }

    public function reads()
    {
        return $this->morphMany('App\Models\Ticket\MessageRead', 'readable');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Ticket\Message', 'customer_id');
    }
}

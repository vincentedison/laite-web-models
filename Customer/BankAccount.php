<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Customer;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function bank()
  {
      return $this->belongsTo('App\Models\General\Bank', 'bank_id');
  }
}

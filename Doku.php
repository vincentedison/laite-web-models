<?php

namespace App\Models;

class Doku
{
    protected $url, $mallid, $sharedkey, $chainmerchant, $purchaseamount, $transidmerchant, $words, 
              $requestdatetime, $currency, $sessionid, $name, $email, $phone, $basket, $paymentchannel;

    const CHANNEL = [
      'CC' => 15,
      'VA' => 36,
      'AF' => 35
    ];
    /**
     * Create a new controller instance.
     * 
     * cart example :
     * [
     *  ['name' => 'Shoes White', 'amount' => 20000.00],
     *  ['name' => 'Admin Fee', 'amount' => 1000.00],
     * ]
     *
     * @return void
     */
    public function __construct($transidmerchant, $customer_name, $customer_phone, $customer_email, $cart, $channel = null, $others = [])
    {
        $this->url = env('DOKU_URL', 'https://apps.myshortcart.com/payment/request-payment/');
        $this->mallid = env('DOKU_MALL_ID');
        $this->sharedkey = env('DOKU_SHARED_KEY');
        $this->chainmerchant = $others['CHAINMERCHANT'] ?? 'NA';
        $this->cart = $cart ?? [];
        $this->amount = $this->getAmount();
        $this->transidmerchant = $transidmerchant;
        $this->currency = $others['CURRENCY'] ?? '360';
        $this->name = $customer_name ?? '';
        $this->phone = $customer_phone ?? '';
        $this->email = $customer_email ?? '';
        $this->paymentchannel = self::CHANNEL[$channel] ?? '';
    }

    public function getAmount() {
      $total_amount = 0.0;
      foreach($this->cart as $c){
        $total_amount += $c['amount'];
      }
      return number_format(round($total_amount), 2, '.', '');
    }

    public function getBasket() {
      $basket = "";
      foreach($this->cart as $i => $c){
        if($i > 0)
          $basket .= ';';
        $basket .= $c['name'] . ',' . $c['amount'] . ',1,' . $c['amount'];
      }
      return $basket;
    }

    public function getWords() { // Myshortcart
      return sha1($this->getAmount() . $this->sharedkey . $this->transidmerchant);
    }

    public function getWordsV2() { // Doku Merchant
      return sha1($this->getAmount() . $this->mallid . $this->sharedkey . $this->transidmerchant);
    }

    public function getSessionId() {
      return session()->getId();
    }

    public function getRequestDateTime() {
      return now()->format('YmdHis');
    }

    public function showForm() { // Myshortcart
      return <<<EOT
      <FORM id="doku-form" METHOD="POST" style="display: none" ACTION="$this->url" >
      <input type="hidden" name="STOREID" value="$this->mallid"/>
      <input type="hidden" name="CHAINMERCHANT" value="$this->chainmerchant"/>
      <input type=hidden name="AMOUNT" value="{$this->getAmount()}">
      <input type=hidden name="PURCHASEAMOUNT" value="{$this->getAmount()}">
      <input type=hidden name="TRANSIDMERCHANT" value="$this->transidmerchant">
      <input type=hidden name="WORDS" value="{$this->getWords()}">
      <input type="hidden" name="REQUESTDATETIME" value="{$this->getRequestDateTime()}">
      <input type="hidden" name="CURRENCY" value="$this->currency">
      <input type="hidden" name="PURCHASECURRENCY" value="$this->currency">
      <input type="hidden" name="SESSIONID" value="{$this->getSessionId()}">
      <input type=hidden name="CNAME" value="$this->name">
      <input type=hidden name="CEMAIL" value="$this->email">
      <input type=hidden name="CCOUNTRY" value="Indonesia">
      <input type=hidden name="SCOUNTRY" value="Indonesia">
      <input type=hidden name="CMPHONE" value="$this->phone">
      <input type=hidden name="BASKET" value="{$this->getBasket()}">
      <input type="hidden" name="PAYMENTMETHODID" value="$this->paymentchannel">
      </FORM>
EOT;

    }

    public function showFormV2() { // Doku Merchant
      return <<<EOT
      <FORM id="doku-form" METHOD="POST" style="display: none" ACTION="$this->url" >
      <input type="hidden" name="MALLID" value="$this->mallid"/>
      <input type="hidden" name="CHAINMERCHANT" value="$this->chainmerchant"/>
      <input type=hidden name="AMOUNT" value="{$this->getAmount()}">
      <input type=hidden name="PURCHASEAMOUNT" value="{$this->getAmount()}">
      <input type=hidden name="TRANSIDMERCHANT" value="$this->transidmerchant">
      <input type=hidden name="WORDS" value="{$this->getWordsV2()}">
      <input type="hidden" name="REQUESTDATETIME" value="{$this->getRequestDateTime()}">
      <input type="hidden" name="CURRENCY" value="$this->currency">
      <input type="hidden" name="PURCHASECURRENCY" value="$this->currency">
      <input type="hidden" name="SESSIONID" value="{$this->getSessionId()}">
      <input type=hidden name="NAME" value="$this->name">
      <input type=hidden name="EMAIL" value="$this->email">
      <input type=hidden name="COUNTRY" value="ID">
      
      <input type=hidden name="MOBILEPHONE" value="$this->phone">
      <input type=hidden name="BASKET" value="{$this->getBasket()}">
      <input type="hidden" name="PAYMENTCHANNEL" value="$this->paymentchannel">
      </FORM>
EOT;

    }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const ACTIVE = [
    1 => "Active",
    2 => "Inactive"
  ];
  
  public function getActiveLabelAttribute()
  {
      return self::ACTIVE[$this->active];
  }
  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }
}

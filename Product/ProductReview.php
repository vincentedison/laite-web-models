<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Product;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const STATUS = [
    0 => "Waiting Review",
    1 => "Reviewed"
  ];
  const WAITING_REVIEW = 0;
  const REVIEWED = 1;

  public function scopeTestimonial($query)
  {
    return $query->whereNull('product_id');
  }

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function product()
  {
      return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }

  public function  getStatusLabelAttribute()
  {
    return self::STATUS[$this->status ?? 0];
  }
}

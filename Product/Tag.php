<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Product;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\SlugableModel;

class Tag extends Model
{
  use LoggableModel, SlugableModel;

  protected $guarded = [];
  protected $visible = ['id'];  
  
  public function products()
  {
      return $this->belongsToMany('App\Models\Product\Product', 'product_tags', 'tag_id', 'product_id');
  }

  public function scopeSlug($query, $slug)
  {
    return $query->where('slug', $slug)->first();
  }
}

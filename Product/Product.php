<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Product;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\SlugableModel;
use Auth;

class Product extends Model
{
  use LoggableModel, SlugableModel;

  protected $guarded = [];
  protected $visible = ['id'];  
  
  public function category()
  {
    return $this->belongsTo('App\Models\Product\Category', 'category_id');
  }

  public function images()
  {
      return $this->hasMany('App\Models\Product\ProductImage', 'product_id');
  }

  public function product_wishlists()
  {
      return $this->belongsToMany('App\Models\Product\Product', 'wishlists', 'product_id', 'customer_id');
  }

  public function getIsWishlistAttribute()
  {
    $customer = Auth::user();
    if ($customer) {
      if ($this->product_wishlists->contains($customer->id)) {
        return true;
      }
    }    
    return false;
  }

  public function getPriceLabelAttribute()
  {
    return $this->master_price - ($this->master_price * ($this->discount/100));
  }

  public function tags()
  {
      return $this->belongsToMany('App\Models\Product\Tag', 'product_tags', 'product_id', 'tag_id');
  }

  public function product_reviews()
  {
      return $this->hasMany('App\Models\Product\ProductReview', 'product_id');
  }

  public function getTotalReviewAttribute()
  {
    return intval($this->product_reviews()->average('rating')) > 0  ? intval($this->product_reviews()->average('rating')) : null;
  }
}

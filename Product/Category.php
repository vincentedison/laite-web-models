<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Product;

use App\Models\Traits\LoggableModel;
use App\Models\Traits\SlugableModel;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  use LoggableModel, SlugableModel;

  protected $guarded = [];
  protected $visible = ['id'];

  public function parent()
  {
    return $this->belongsTo('App\Models\Product\Category', 'parent_id');
  }

  public function childs()
  {
    return $this->hasMany('App\Models\Product\Category', 'parent_id');
  }
  
  public function products()
  {
    return $this->hasMany('App\Models\Product\Product', 'category_id');
  }

  public function getActiveLabelAttribute()
  {
    return $this->active  == 1 ? "active" : "inactive";
  }
  
  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }

  public function getFullnameAttribute()
    {
        $fullname = $this->name;
        $object = clone($this);
        while($object->parent_id != null)
        {
            $object = clone $object->parent;
            $fullname =  $object->name . ' / ' . $fullname;
        }
        return $fullname;
    }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Shipping;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class ShippingMethod extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];

  public function logistic()
  {
      return $this->belongsTo('App\Models\Shipping\Logistic', 'logistic_id');
  }

  public function getActiveLabelAttribute()
  {
    return $this->active  == 1 ? "active" : "inactive";
  }

  public function getShippingLabelAttribute()
  {
    return $this->logistic_id ? $this->logistic->name."|".$this->name : $this->name;
  }
  
  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }
}

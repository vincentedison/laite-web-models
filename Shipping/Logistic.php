<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Shipping;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Logistic extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const CODES = [
    "jne" => "JNE",
    "pos" => "POS",
    "wahana" => "WAHANA",
    "jnt" => "J&T",
    // "pickup" => "Pickup",
  ];

  public function shipping_methods()
  {
    return $this->hasMany('App\Models\Shipping\ShippingMethod', 'logistic_id');
  }
  
  public function getActiveLabelAttribute()
  {
    return $this->active  == 1 ? "active" : "inactive";
  }
  
  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }
}

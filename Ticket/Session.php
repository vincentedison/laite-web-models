<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Ticket;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Customer\Customer;
use App\Models\Ticket\Agent;

class Session extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const STATUS = [  
    1 => "Open",
    2 => "Closed",
  ];

  const RUNNING = 1;
  const END = 2;
  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function topic()
  {
      return $this->belongsTo('App\Models\Ticket\Topic', 'topic_id');
  }

  public function getStatusLabelAttribute()
  {
    return self::STATUS[$this->status ?? 0];
  }

  public function messages()
  {
      return $this->hasMany('App\Models\Ticket\Message', 'session_id');
  }

  public function agents()
  {
      return $this->belongsToMany('App\Models\Ticket\Agent', 'agent_sessions', 'session_id', 'agent_id');
  }

  // public function getJoinedAttribute()
  // {
  //   return $this->agents()->pluck('id');
  // }

  public function set_customer_read(Customer $customer)
  {
    $agent_read_ids = $this->messages()->whereHas('message_reads', function($q){
        $q->where('readable_type', 'App\Models\Ticket\Agent');
    })->pluck('id');

    $customer_read_ids = $this->messages()->whereHas('message_reads', function($q){
        $q->where('readable_type', 'App\Models\Customer\Customer');
    })->pluck('id');    

    $messages = $this->messages()->whereIn('id', $agent_read_ids)->whereNotIn('id', $customer_read_ids)->get();
    
    if($messages->count() > 0){
      foreach ($messages as $message) {
        $read     = new MessageRead;
        $read->message_id = $message->id;
        $customer->reads()->save($read);
      }
    }
  }

  public function set_agent_read(Agent $agent)
  {
    $agent_read_ids = $this->messages()->whereHas('message_reads', function($q){
        $q->where('readable_type', 'App\Models\Ticket\Agent');
    })->pluck('id');

    $customer_read_ids = $this->messages()->whereHas('message_reads', function($q){
        $q->where('readable_type', 'App\Models\Customer\Customer');
    })->pluck('id');    

    $messages = $this->messages()->whereIn('id', $customer_read_ids)->whereNotIn('id', $agent_read_ids)->get();
    
    if($messages->count() > 0){
      foreach ($messages as $message) {
        $read     = new MessageRead;
        $read->message_id = $message->id;
        $agent->reads()->save($read);
      }
    }
  }
}

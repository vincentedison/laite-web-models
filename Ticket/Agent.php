<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Ticket;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Agent extends Authenticatable
{
  use Notifiable, LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function topics()
  {
      return $this->belongsToMany('App\Models\Ticket\Topic', 'agent_topics', 'agent_id', 'topic_id');
  }

  public function reads()
  {
      return $this->morphMany('App\Models\Ticket\MessageRead', 'readable');
  }

  public function messages()
  {
      return $this->hasMany('App\Models\Ticket\Message', 'agent_id');
  }

  public function sessions()
  {
      return $this->belongsToMany('App\Models\Ticket\Session', 'agent_sessions', 'agent_id', 'session_id');
  }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Ticket;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];
  
  public function category()
  {
      return $this->belongsTo('App\Models\Ticket\TopicCategory', 'topic_category_id');
  }

  public function agents()
  {
      return $this->belongsToMany('App\Models\Ticket\Agent', 'agent_topics', 'topic_id', 'agent_id');
  }

  public function sessions()
  {
      return $this->hasMany('App\Models\Ticket\Session', 'topic_id');
  }
}

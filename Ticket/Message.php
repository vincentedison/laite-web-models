<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Ticket;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];
  
  public function message_reads()
  {
      return $this->hasMany('App\Models\Ticket\MessageRead', 'message_id');
  }

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function agent()
  {
      return $this->belongsTo('App\Models\Ticket\Agent', 'agent_id');
  }

  public function getAgentReadStatusAttribute()
  {
    $status = false;    
    $exits = $this->message_reads()->where([['readable_type', 'App\Models\Ticket\Agent'], ['message_id', $this->id]])->first();
    if ($exits) {
      $status = true;
    }
    return $status;
  }

  public function getCustomerReadStatusAttribute()
  {
    $status = false;
    $exits = $this->message_reads()->where([['readable_type', 'App\Models\Customer\Customer'], ['message_id', $this->id]])->first();
    if ($exits) {
      $status = true;
    }
    return $status;
  }

}

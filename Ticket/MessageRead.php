<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Ticket;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class MessageRead extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id']; 
  
  public function message()
  {
      return $this->belognsTo('App\Models\Ticket\Message', 'message_id');
  }   

  public function readable()
  {
      return $this->morphTo();
  }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Ticket;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class TopicCategory extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function topics()
  {
      return $this->hasMany('App\Models\Ticket\Topic', 'topic_category_id');
  }
}

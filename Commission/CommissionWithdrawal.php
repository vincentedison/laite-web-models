<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Commission;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Commission\CommissionHistory;
use Auth;

class CommissionWithdrawal extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  protected static function boot()
  {
    parent::boot();

    static::saved(function($object) {
      $history = new CommissionHistory;
      $history->customer_id = $object->customer_id;
      $history->description = "Penarikan ke Rekening ".$object->bank_account->account_name." - ".$object->bank_account->account_number;
      $history->amount = $object->amount;
      $object->commission_histories()->save($history);
    });
  }

  const STATE = [
    1 => "New Request",
    2 => "Proccessed"
  ];

  const NEW_REQUEST = 1;
  const PROCCESSED  = 2;

  public function scopeNewRequest($query)
  {
    return $query->where('withdrawal_state', self::PROCCESSING);
  }

  public function commission_histories()
  {
      return $this->morphMany('App\Models\Commission\CommissionHistory', 'originator');
  }

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function bank_account()
  {
      return $this->belongsTo('App\Models\Customer\BankAccount', 'bank_account_id');
  }
  public function getStateLabelAttribute()
  {
    return self::STATE[$this->withdrawal_state];
  }
}

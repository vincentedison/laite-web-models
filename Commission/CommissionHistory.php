<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Commission;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class CommissionHistory extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  protected static function boot()
  {
    parent::boot();

    static::saved(function($object) {
      $customer = $object->customer;
      $customer->current_commission +=  $object->amount;
      $customer->save();    
    });
  }

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function originator()
  {
      return $this->morphTo();
  }
}

<?php

/**
  * Copyright 2018 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Traits;

trait SlugableModel
{
    public function buildSlugFrom($attribute = 'name', $scope = null)
    {
        $slug_candidate = '';
        $exists = true;
        $counter = 0;
        while(true) {
            $slug_candidate = str_slug($this->{$attribute}, '-') . ($counter > 0 ? ('-' . $counter) : '') ;
            $same_slug_obj = self::withoutGlobalScopes()->where('slug', $slug_candidate);
            if($this->exists()) {
                $same_slug_obj = $same_slug_obj->where('id', '<>', $this->id);
            }
            if($scope) {
                foreach($scope as $sck => $scv) {
                    $same_slug_obj = $same_slug_obj->where($sck, $scv);
                }
            }
            $exists = $same_slug_obj->count();
            $counter += 1;
            if(!$exists)
                break;
        }
        $this->slug = $slug_candidate;
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
}
<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Point;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PointExchange extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];   
  
  protected static function boot()
  {
    parent::boot();

    static::saved(function($object) {
      $history = new PointHistory;
      $history->customer_id = $object->customer_id;
      $history->description = "Penukaran Point ".$object->reward ? "dengan ".$object->reward->name : null;
      $history->amount = $object->price;
      $object->point_histories()->save($history);

      if($object->reward){
        $object->reward->stock -= 1;
        $object->reward->save();
      }
    });
  }

  const AVAILABLE = 1;

  public function scopeAvailable($query)
  {
    return $query->where('exchange_state', self::AVAILABLE)->where('expired_at', '>=' , Carbon::now());
  }

  public function point_histories()
  {
      return $this->morphMany('App\Models\Point\PointHistory', 'originator');
  }

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function reward()
  {
      return $this->belongsTo('App\Models\Point\Reward', 'reward_id');
  }
}

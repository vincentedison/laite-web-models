<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Point;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];

  const REWARD_TYPE = [
    1 => "Exchange",
    2 => "General",
  ];

  const DISCOUNT_TARGET = [
    1 => "Item",
    2 => "Shipping",
  ];

  const DISCOUNT_AMOUNT_TYPE = [
    1 => "Percentage",
    2 => "Fixed",
  ];

  const REWARD_TYPE_EXCHANGE = 1;
  const REWARD_TYPE_GENERAL = 2;
  const DISCOUNT_TARGET_ITEM = 1; 
  const DISCOUNT_TARGET_SHIPPING = 2; 
  const DISCOUNT_PERCENTAGE = 1; 
  const DISCOUNT_FIXED = 2; 

  public function scopeAvailable($query)
  {
    return $query->where('stock', '>', 0);
  }

  public function getRewardTypeLabelAttribute()
  {
    return self::REWARD_TYPE[$this->reward_type];
  }

  public function getDiscountTargetLabelAttribute()
  {
    return self::DISCOUNT_TARGET[$this->discount_target];
  }

  public function getDiscountAmountTypeLabelAttribute()
  {
    return self::DISCOUNT_AMOUNT_TYPE[$this->discount_amount_type];
  }

  public function getExpirationDaysLabelAttribute()
  {
    return $this->expiration_days." days";
  }
}

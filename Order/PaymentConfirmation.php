<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Order;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PaymentConfirmation extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const STATUS = [
    0 => "Draft",
    1 => "Approved",
    2 => "Rejected",
  ];

  const STATUS_DRAFT = 0;
  const STATUS_APPROVE = 1;
  const STATUS_REJECTED = 2;

  public function getStatusLabelAttribute()
  {
    return self::STATUS[$this->status ?? 0];
  }

  public function order()
  {
      return $this->belongsTo('App\Models\Order\Order', 'order_id');
  }

  public function bank()
  {
      return $this->belongsTo('App\Models\General\Bank', 'bank_id');
  }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Order;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];
  
  protected static function boot()
  {
      parent::boot();

      static::saving(function($object) {
          $object->subtotal = $object->quantity * $object->price;
      });
      static::saved(function($object) {
          if($object->quantity <= 0) {
              $object->delete();
              if($object->order->lines()->count() <= 0) {
                  $object->order->delete();
              }
          }
      });
  }

  public function order()
  {
      return $this->belongsTo('App\Models\Order\Order', 'order_id');
  }

  public function product()
  {
      return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }
}

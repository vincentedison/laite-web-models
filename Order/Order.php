<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Order;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\ProductReview;
use App\Models\Customer\Customer;
use App\Models\Warehouse;
use H;
use App\Models\Point\Reward;

class Order extends Model
{
  use LoggableModel;
 
  protected $guarded = [];
  protected $visible = ['id'];    

    protected static function boot()
    {
        parent::boot();

        static::creating(function($object) {
            $object->order_state = 1;
            $warehouse = Warehouse::default()->first();
            $object->warehouse_id = $warehouse ? $warehouse->id : null;

            $address = $object->customer->addresses()->first();
            $object->recipient_subdistrict_id = $address ? $address->subdistrict_id : null;
            $object->recipient_name = $address ? $address->recipient_name : null;
            $object->recipient_address = $address ? $address->address : null;
            $object->recipient_mobile_number = $address ? $address->mobile_number : null;
            $object->recipient_zip_code = $address ? $address->zip_code : null;
        });

        static::saving(function($object) {
            if($object->order_state == self::STATE_NEW) {
                $lines = $object->lines()->get();
                $total = 0.00;
                $count = 0;
                $weight = 0;
                foreach($lines as $line) {
                    $total += $line->subtotal;
                    $count += $line->quantity;
                    $weight += $line->product->weight;
                }
    
                $object->item_count = $count;
                $object->item_total = $total;
                $object->total_weight = $weight;        

                if($object->coupon_code){
                    $object->get_voucher($object->coupon_code);
                }

                $total += $object->shipping_cost;
                $total += $object->insurance_cost;
                $total -= $object->discount_amount;

                $object->order_total = $total;
                
            }
            
            if($object->isDirty('shipping_method_id')) {
                if ($object->shipping_method->logistic->code == "pickup") {
                    while(true) {                        
                        $number_candidate = H::generateNumber(9, 'VW');
                        if(self::where('shipping_waybill', $number_candidate)->count() == 0) {                        
                            $object->shipping_waybill = $number_candidate;
                            break;
                        }
                    }
                }else{
                    $object->shipping_waybill = null;
                }
            }        
        });

        static::saved(function($object) {            
            if($object->isDirty('order_state') && $object->order_state == self::STATE_COMPLETED) {
                $lines = $object->lines()->get();
                foreach($lines as $line) {                    
                    $review = new ProductReview;
                    $review->product_id = $line->product->id;
                    $review->customer_id = $object->customer_id;
                    $review->save();
                }                
            }
        });
    }

    public function lines()
    {
        return $this->hasMany('App\Models\Order\OrderLine', 'order_id');
    }

    public function states_changes()
    {
        return $this->hasMany('App\Models\Order\OrderStateChange', 'order_id');
    }

    public function payment_confirmations()
    {
        return $this->hasMany('App\Models\Order\PaymentConfirmation', 'order_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
    }

    public function payment_method()
    {
        return $this->belongsTo('App\Models\General\PaymentMethod', 'payment_method_id');
    }

    const ORDER_STATE = [
        1 => 'new order',
        2 => 'Payment process',
        3 => 'Order processed',
        4 => 'Order delivered',
        5 => 'Order completed',
        6 => 'Order canceled by seller',
    ];

    const STATE_NEW = 1;
    const STATE_PAYMENT_PROCESS = 2;
    const STATE_PROCESSED = 3;
    const STATE_DELIVERED = 4;
    const STATE_COMPLETED = 5;
    const STATE_CANCELLED_BY_SELLER = 6;

    const PAYMENT_STATE = [
        0 => 'waiting for payment',
        1 => 'paid',
    ];

    const WAITING_FOR_PAYMENT = 0;
    const PAID = 1;

    public function getStateLabelAttribute()
    {
        $state = self::ORDER_STATE[$this->order_state];
        return H::humanizeString($state);
    }

    public function getPaymentStateLabelAttribute()
    {
        $state = self::PAYMENT_STATE[$this->payment_state ?? 0];
        return H::humanizeString($state);
    }

    public function scopeNumber($query, $order_number)
    {
        return $query->where('order_number', $order_number)->first();
    }

    public function scopeCustomerIs($query, $customer_id)
    {
        return $query->where('customer_id', $customer_id);
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id');
    }

    public function shipping_subdistrict()
    {
        return $this->belongsTo('App\Models\General\Address\Subdistrict', 'recipient_subdistrict_id');
    }

    public function shipping_method()
    {
        return $this->belongsTo('App\Models\Shipping\ShippingMethod', 'shipping_method_id');
    }

    public function get_voucher($code)
    {
        $customer = $this->customer;
        $voucher = Reward::where('reward_code', $code)->first();
        $discount_amount = 0;
        if ($voucher) {
            $voucher_valid = $customer->point_exchanges()->available()->where('reward_id', $voucher->id)->first();
            if ($voucher_valid) {
                if ($voucher->discount_target == Reward::DISCOUNT_TARGET_ITEM) {
                    if ($voucher->discount_amount_type == Reward::DISCOUNT_PERCENTAGE) {
                        $discount_amount = $this->item_total * ($voucher->discount_amount/100);
                    }
                
                    if ($voucher->discount_amount_type == Reward::DISCOUNT_FIXED) {
                        if ($this->item_total >= $voucher->discount_amount) {
                            $discount_amount = $voucher->discount_amount;
                        }else{
                            $discount_amount = $this->item_total;
                        }
                    }
                }
                    
                if ($voucher->discount_target == Reward::DISCOUNT_TARGET_SHIPPING) {
                    if ($voucher->discount_amount_type == Reward::DISCOUNT_PERCENTAGE) {
                        $discount_amount = $this->shipping_cost * ($voucher->discount_amount/100);
                    }
                
                    if ($voucher->discount_amount_type == Reward::DISCOUNT_FIXED) {                    
                        if ($this->shipping_cost >= $voucher->discount_amount) {
                            $discount_amount = $voucher->discount_amount;
                        }else{
                            $discount_amount = $this->shipping_cost;
                        }
                    }
                }

                $this->coupon_code = $code;
                $this->discount_target = $voucher->discount_target;
                $this->discount_amount = $discount_amount;
            }
        }
    }

    const SHIPPING_STATUS = [
        0 => 'not shipped',
        1 => 'shipped',
        2 => 'delivered'
    ];

    const SHIPPING_STATUS_NOT_SHIPPED = 0;
    const SHIPPING_STATUS_SHIPPED = 1;
    const SHIPPING_STATUS_DELIVERED = 2;
    
    public function getShippingStatusLabelAttribute()
    {
        $state = self::SHIPPING_STATUS[$this->shipping_status ?? 0];
        return H::humanizeString($state);
    }

    const DISCOUNT_TARGET = [
        1 => "Item",
        2 => "Shipping",
    ];

    public function getDiscountTargetLabelAttribute()
    {
      return self::DISCOUNT_TARGET[$this->discount_target];
    }

    public function getTotalWeightLabelAttribute()
    {
      return $this->total_weight >= 1000 ? str_replace('.', ',', $this->total_weight/1000) ." Kg" : $this->total_weight ." Gram";
    }

    public function commission_histories()
    {
        return $this->morphMany('App\Models\Commission\CommissionHistory', 'originator');
    }

    public function bonus_histories()
    {
        return $this->morphMany('App\Models\Bonus\BonusHistory', 'originator');
    }

    public function point_histories()
    {
        return $this->morphMany('App\Models\Point\PointHistory', 'originator');
    }

    public function calculateCommissionForReseller()
    {        
        if ($this->customer->customer_type != Customer::CUSTOMER_TYPE_RESELLER) {
            if ($this->customer->reseller) {                 
                if ($this->customer->reseller->id == $this->customer->reference_id) {
                    $this->customer->reseller->setCommissionHistory($this->commissionForDirectReseller, "Komisi dari belanja ".$this->customer->name, $this);
                    // $this->warehouse->setCommissionHistory
                    if ($this->item_total >= 50000) {
                        $this->customer->setPointHistory(($this->netTotalAfterDiscountPercentage - $this->commissionForDirectReseller) / 2000, "Point belanja dari belanja ".$this->order_number, $this);
                        $this->customer->setBonusHistory(($this->netTotalAfterDiscountPercentage - $this->commissionForDirectReseller) / 2000, "Point hadiah dari belanja ".$this->order_number, $this);
                    }
                }else{
                    $this->customer->reseller->setCommissionHistory($this->commissionForNonDirectReseller, "Komisi dari belanja ".$this->customer->name, $this);
                    if ($this->item_total >= 50000) {
                        $this->customer->setPointHistory(($this->netTotalAfterDiscountPercentage - $this->commissionForNonDirectReseller) / 2000, "Point belanja dari belanja ".$this->order_number, $this);
                        $this->customer->setBonusHistory(($this->netTotalAfterDiscountPercentage - $this->commissionForNonDirectReseller) / 2000, "Point hadiah dari belanja ".$this->order_number, $this);
                    }
                }
            }
        }else {
            $this->customer->setPointHistory($this->netTotalAfterDiscountPercentage / 2000, "Point belanja dari belanja ".$this->order_number, $this);
            $this->customer->setBonusHistory($this->netTotalAfterDiscountPercentage / 2000, "Point hadiah dari belanja ".$this->order_number, $this);
        }        
    }

    public function getCommissionForDirectResellerAttribute()
    {
        $commision_percentage = 0.00;
        if ($this->net_discount_percentage <= 10) {
            $commision_percentage = 15.00;
        }else if ($this->net_discount_percentage <= 15) {
            $commision_percentage = 10.00;
        }else if ($this->net_discount_percentage <= 20) {
            $commision_percentage = 5.00;
        }else if ($this->net_discount_percentage <= 25) {
            $commision_percentage = 2.00;
        }
    
        return ($commision_percentage/100) * $this->NetTotalAfterDiscountPercentage;
    }

    public function getCommissionForNonDirectResellerAttribute()
    {
        $commision_percentage = 0.00;
        if ($this->net_discount_percentage <= 20) {
            $commision_percentage = 5.00;
        }else if ($this->net_discount_percentage <= 25) {
            $commision_percentage = 2.00;
        }
        
        return ($commision_percentage/100) * $this->netTotalAfterDiscountPercentage;
    }

    public function getNetDiscountPercentageAttribute()
    {
        return intval(($this->dicount / $this->item_total ) * 100);
    }

    public function getNetTotalAfterDiscountPercentageAttribute()
    {
        return $this->item_total - $this->discount;
    }

    public function voucher()
    {
        return $this->belongsTo('App\Models\Point\Reward', 'coupon_code', 'reward_code');
    }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Order;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function customer()
  {
      return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function product()
  {
      return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }
}

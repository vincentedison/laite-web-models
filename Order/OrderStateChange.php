<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Order;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class OrderStateChange extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    
  
  public function order()
  {
      return $this->belongsTo('App\Models\Order\Order', 'order_id');
  }
}

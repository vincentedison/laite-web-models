<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Bonus;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];
  
  public function scopeAvailable($query)
  {
    return $query->where('stock', '>', 0);
  }
}

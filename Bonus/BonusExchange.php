<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Bonus;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bonus\BonusHistory;
use App\Models\Bonus\Reward;
use Auth;

class BonusExchange extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];  

  protected static function boot()
  {
    parent::boot();

    static::saved(function($object) {
      $history = new BonusHistory;
      $history->customer_id = $object->customer_id;
      $history->amount = $object->price;
      $history->description = "Penukaran Point Hadiah ".$object->reward ? "dengan ".$object->reward->name : null;
      $object->bonus_histories()->save($history);
      if ($object->reward) {
        $reward = $object->reward;
        $reward->stock -= 1;
        $reward->save();
      }
    });
  }

  const STATE = [
    1 => 'New Request',
    2 => 'Proccesing',
    3 => 'Bonus Shipping',
    4 => 'Bonus Delivered',
    5 => 'Bonus Completed'
  ];

  const STATE_NEW = 1;
  const STATE_PROCESSING = 2;
  const STATE_DELIVERED = 3;
  const STATE_COMPLETED = 4;

  public function scopeNewRequest($query)
  {
    return $query->where('exchange_state', self::STATE_NEW);
  }

  public function scopeProccessing($query)
  {
    return $query->where('exchange_state', self::STATE_PROCESSING);
  }

  public function scopeDelivered($query)
  {
    return $query->where('exchange_state', self::STATE_DELIVERED);
  }

  public function scopeCompleted($query)
  {
    return $query->where('exchange_state', self::STATE_COMPLETED);
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function customer_address()
  {
    return $this->belongsTo('App\Models\Customer\CustomerAddress', 'customer_address_id');
  }

  public function bonus_histories()
  {
      return $this->morphMany('App\Models\Bonus\BonusHistory', 'originator');
  }

  public function reward()
  {
      return $this->belongsTo('App\Models\Bonus\Reward', 'reward_id');
  }
}

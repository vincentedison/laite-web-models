<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Forum;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class DiscussionReply extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    


  public function replyable()
  {
      return $this->morphTo();
  }

  public function replies()
  {
    return $this->morphMany(DiscussionReply::class, 'replyable');
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }
}

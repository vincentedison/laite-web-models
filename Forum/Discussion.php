<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Forum;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];

  public function forum()
  {
      return $this->belongsTo(Forum::class, 'forum_id');
  }

  public function replies()
  {
    return $this->morphMany(DiscussionReply::class, 'replyable');
  }

  public  function getForumLabelAttribute()
  {
    return $this->forum ? $this->forum->name : '-';
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }
}

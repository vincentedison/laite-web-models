<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Forum;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];   
  
  public function discussions()
  {
    return $this->hasMany(Discussion::class, 'forum_id');
  }
}

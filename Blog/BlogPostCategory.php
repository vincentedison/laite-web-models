<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Blog;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class BlogPostCategory extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Blog;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const STATUS = [
    1 => "Active",
    2 => "Inactive"
  ];
  public function getStatusLabelAttribute()
  {
      return self::STATUS[$this->status];
  }

  public function categories()
  {
    return $this->belongsToMany('App\Models\Blog\BlogCategory', 'blog_post_categories', 'blog_post_id', 'blog_category_id');
  }

  public function user()
  {
      return $this->belongsTo('App\Models\User', 'user_id');
  }
  public function scopeActive($query)
  {
    return $query->where('status', 1);
  }
}

<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models\Blog;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  const ACTIVE = [
    1 => "Active",
    2 => "Inactive"
  ];
  public function getActiveLabelAttribute()
  {
      return self::ACTIVE[$this->active];
  }

  public function posts()
  {
      return $this->belongsToMany('App\Models\Blog\BlogPost', 'blog_post_categories', 'blog_category_id', 'blog_post_id');
  }
}

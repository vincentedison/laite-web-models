<?php

/**
  * Copyright 2019 Luxodev Indonesia. All Rights Reserved.
  */

namespace App\Models;

use App\Models\Traits\LoggableModel;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
  use LoggableModel;

  protected $guarded = [];
  protected $visible = ['id'];    

  public function subdistrict()
  {
      return $this->belongsTo('App\Models\General\Address\Subdistrict', 'subdistrict_id');
  }

  public function getInternalLabelAttribute()
  {
    return $this->internal ? "Internal" : "External";
  }

  public function scopeActive($query)
  {
    return $query->where('active', 1);
  }

  public function scopeDefault($query)
  {
    return $query->where('default', 1);
  }
}
